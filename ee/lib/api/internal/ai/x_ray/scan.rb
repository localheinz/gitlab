# frozen_string_literal: true

# rubocop: disable Gitlab/AvoidGitlabInstanceChecks -- This feature is developed on extremely short notice,
# so I follow existing code patterns in code suggestions AddOn Flow.
module API
  module Internal
    module Ai
      module XRay
        class Scan < ::API::Base
          feature_category :code_suggestions

          helpers ::API::Ci::Helpers::Runner
          helpers ::API::Helpers::CloudConnector

          before do
            authenticate_job!
            not_found! unless x_ray_enabled_on_instance?
            unauthorized! unless x_ray_available?
          end

          helpers do
            include ::Gitlab::Utils::StrongMemoize

            def x_ray_enabled_on_instance?
              return true if ::Gitlab.org_or_com?
              return false unless ::License.feature_available?(:code_suggestions)

              if ::CodeSuggestions::SelfManaged::SERVICE_START_DATE.past?
                ::GitlabSubscriptions::AddOnPurchase
                  .for_gitlab_duo_pro
                  .any?
              else # Before service start date
                # TODO: Remove this else branch after the service start date
                ::Gitlab::CurrentSettings.instance_level_code_suggestions_enabled
              end
            end

            def x_ray_available?
              if Gitlab.org_or_com?
                gitlab_duo_pro_add_on?
              else
                ai_gateway_token.present?
              end
            end

            def gitlab_duo_pro_add_on?
              ::GitlabSubscriptions::AddOnPurchase
                .for_gitlab_duo_pro
                .by_namespace_id(current_namespace.id)
                .active
                .any?
            end

            def model_gateway_headers(headers, gateway_token)
              {
                'X-Gitlab-Instance-Id' => ::Gitlab::CurrentSettings.uuid.presence || GITLAB_INSTANCE_UUID_NOT_SET,
                'X-Gitlab-Host-Name' => Gitlab.config.gitlab.host,
                'X-Gitlab-Realm' => gitlab_realm,
                'X-Gitlab-Authentication-Type' => 'oidc',
                'Authorization' => "Bearer #{gateway_token}",
                'Content-Type' => 'application/json',
                'User-Agent' => headers["User-Agent"] # Forward the User-Agent on to the model gateway
              }.merge(saas_headers).transform_values { |v| Array(v) }
            end

            def saas_headers
              return {} unless Gitlab.com?

              {
                'X-Gitlab-Saas-Namespace-Ids' => [current_namespace.id.to_s]
              }
            end

            def current_namespace
              current_job.namespace
            end
            strong_memoize_attr :current_namespace

            def ai_gateway_token
              ::CloudConnector::AccessService.new.access_token([:code_suggestions], gitlab_realm)
            end
            strong_memoize_attr :ai_gateway_token
          end

          namespace 'internal' do
            resource :jobs do
              params do
                requires :id, type: Integer, desc: %q(Job's ID)
                requires :token, type: String, desc: %q(Job's authentication token)
              end
              post ':id/x_ray/scan' do
                workhorse_headers =
                  Gitlab::Workhorse.send_url(
                    File.join(::CodeSuggestions::Tasks::Base.base_url, 'v1', 'x-ray', 'libraries'),
                    body: params.except(:token, :id).to_json,
                    headers: model_gateway_headers(headers, ai_gateway_token),
                    method: "POST"
                  )

                header(*workhorse_headers)
                status :ok
              end
            end
          end
        end
      end
    end
  end
end
# rubocop: enable Gitlab/AvoidGitlabInstanceChecks
